# ORID
## O:
- Got familiar with code review, an activity to ensure code quality through reviewing each other's codes together and markdown pros & cons for code optimization.
- Held our first code review and learned a lot like using more StringFormat & StringBuilder instead of connect strings directly, and also the power and elegancy of steam API.
- Studied and practice frequently used Java stream api.
- Managed to know about JAVA Object Oriented programing. Understood 3 main concepts: Encapsulation, Inheritance and Polymorphism. Later enhanced our understanding through driving cars Java demo.
## R:

- I was both satisfied and pleasant.
## I:

- The introduction and practice of steam api really help significantly in actual programming.
- With adequate practice, my comprehension on JAVA OOP has deepen a lot.

## D:

- Keep in mind to pay attention to maintain good coding style and make good use of stream api and other post JAVA8 new feature.
- Bear object oriented programing in mind and keep execute it in future.