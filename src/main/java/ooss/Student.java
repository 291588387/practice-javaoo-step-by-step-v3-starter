package ooss;

import java.util.Objects;

public class Student extends Person {
    private Klass klass;

    public Klass getKlass() {
        return klass;
    }

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        String introduction = super.introduce() + " I am a student. ";
        if (this.klass != null) {
            if (this.klass.isLeader(this)) {
                introduction += "I am the leader of class " + klass.getNumber() + ".";
            } else if (!this.klass.isLeader(this) ) {
                introduction += " I am in class " + this.klass.getNumber() + ".";
            }
        }
        return introduction;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (Objects.isNull(this.klass)) return false;
        return this.klass.equals(klass);
    }

    @Override
    public void sayKnowLeader(Klass klass) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n", this.getName(), klass.getNumber(), klass.getLeader().getName());
    }
}
