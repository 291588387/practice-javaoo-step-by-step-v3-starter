package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {


    private int number;

    private Student leader;

    private List<Person> attachedPerson = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public int getNumber() {
        return number;
    }

    public Student getLeader() {
        return leader;
    }

    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            this.leader = student;
            this.notifyAttachedPeople();
        } else {
            System.out.println("It is not one of us.");
        }
    }

    public boolean isLeader(Student student) {
        if (Objects.isNull(student)) return false;
        return Objects.equals(this.leader, student);
    }

    public void attach(Person person) {
        if ( !attachedPerson.contains(person) ) {
            attachedPerson.add(person);
        }
    }

    private void notifyAttachedPeople() {
        attachedPerson.forEach(person -> {
            person.sayKnowLeader(this);
        });
    }
}
