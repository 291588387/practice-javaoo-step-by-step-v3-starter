package ooss;

import com.sun.tools.javac.util.StringUtils;
import org.graalvm.util.CollectionsUtil;

import java.util.*;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klassList = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    @Override
    public String introduce() {
        StringBuilder introduction = new StringBuilder(super.introduce() + " I am a teacher.");
        if (!this.klassList.isEmpty()) {
            introduction.append(" I teach Class ");
            for (int i = 0 ; i < klassList.size() ; i++) {
                if (i != klassList.size() - 1) {
                    introduction.append(klassList.get(i).getNumber()).append(", ");
                } else {
                    introduction.append(klassList.get(i).getNumber()).append(".");
                }
            }
        }
        return introduction.toString();
    }

    public void assignTo(Klass klass) {
        if (!belongsTo(klass)) {
            klassList.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return klassList.contains(klass);
    }

    public boolean isTeaching (Student student) {
        return klassList.contains(student.getKlass());
    }

    @Override
    public void sayKnowLeader(Klass klass) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", this.getName(), klass.getNumber(), klass.getLeader().getName());
    }
}
